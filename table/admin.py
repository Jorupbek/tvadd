from django.contrib import admin
from .models import Choice, Post


admin.site.register(Choice)
admin.site.register(Post)
