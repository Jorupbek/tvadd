from django import forms
from .models import Post


class CreateField(forms.ModelForm):
    date = forms.CharField(max_length=255,
                           widget=forms.TextInput(
                               attrs={'class': 'form-control datepicker-here', 'data-multiple-dates': '100',
                                      'aria-label': 'Todo', 'data-multiple-dates-separator': ', ',
                                      'data-position': 'top left'}))

    class Meta:
        model = Post
        fields = ['choice', 'text', 'date']



class NameForm(forms.Form):
    author_name = forms.CharField(max_length=100)
    text = forms.CharField(max_length=255)
    post_dates = forms.CharField(max_length=255)


